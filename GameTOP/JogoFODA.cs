using System;
using GameTOP.Lib;

namespace GameTOP
{
    public class JogoFODA
    {
        private readonly Jogador1 _jogador1;
        private readonly Jogador2 _jogador2;

        public JogoFODA(Jogador1 jogador1, Jogador2 jogador2)
        {
            _jogador1 = jogador1;
            _jogador2 = jogador2;
        }

        public void IniciarJogo()
        {
            Console.Write(_jogador1.Corre()); 
            Console.Write(_jogador1.Chuta());
            Console.Write(_jogador1.Passa());
            //
            Console.Write("\n PROXIMO JOGADOR \n");
            //
            Console.Write(_jogador2.Corre());
            Console.Write(_jogador2.Chuta());
            Console.Write(_jogador2.Passa());
        }
    }
}